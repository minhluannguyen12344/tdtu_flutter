mixin CommonValidation {
  String? validateEmail(String? value) {
    if (!value!.contains('@')) {
      return 'Pls input valid email.';
    }
    return null;
  }

  String? validatePassword(String? value) {
    if (value!.length < 8) {
      return 'Password must be at least 8 characters.';
    }
    if (value == null || value.isEmpty) {
      return 'Pls enter password.';
    }
    if (!value.contains(RegExp(
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$'))) {
      return 'Password must be at least 1 upper case,1 normal and 1 special and 1 Numeric Number character.';
    }
    return null;
  }
}
